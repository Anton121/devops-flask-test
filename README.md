# Тестовое задание

Steps:

1. git clone https://gitlab.com/Anton121/devops-flask-test.git

2. sudo chown -R 472:472 conf/grafana

3. sudo chown -R 65534:65534 conf/prometheus

4. docker-compose up -d

5. import grafana.json dashboard in grafna web interface

6. test/review results:

   ab -c 100 -s 1 -t 10 http://localhost:5000/

   ab -c 100 -s 1 -t 20 http://localhost:5000/

![Benchmark](test.png)
